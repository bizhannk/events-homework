const divEl = document.querySelector('#new-section');
const sectionEl = document.createElement('section');

sectionEl.className = 'join-us';

sectionEl.innerHTML = `
  <h2>Join Our Program</h2>
  <p class="subheading">Sed do eiusmod tempor incididunt 
  ut labore et dolore magna aliqua.</p>
  <form>
    <input type="email" id="email" placeholder="Email"/>
    <button class="btn" type="submit">Subscribe</button>
  </form>
`;

const loadSection = () => {
  divEl.append(sectionEl);
  const form = document.querySelector('form');
  const email = document.querySelector('#email');
  form.addEventListener('submit', (e) => {
    e.preventDefault();
    console.log(email.value);
  });
};

window.addEventListener('load', loadSection);
